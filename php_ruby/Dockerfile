FROM centos:centos6

RUN yum -y update

# タイムゾーンの設定
RUN sed -i '/^ZONE/d' /etc/sysconfig/clock \
 && sed -i '/^UTC/d' /etc/sysconfig/clock \
 && echo 'ZONE="Asia/Tokyo"' >> /etc/sysconfig/clock \
 && echo 'UTC="false"' >> /etc/sysconfig/clock \
 && cp -p /usr/share/zoneinfo/Asia/Tokyo /etc/localtime

# 日本語環境
RUN yum -y groupinstall "Japanese Support" \
 && localedef -f UTF-8 -i ja_JP ja_JP.utf8 \
 && echo 'LANG="ja_JP.UTF-8"' > /etc/sysconfig/i18n

# epel, remiリポジトリ
RUN rpm -ivh http://ftp.riken.jp/Linux/fedora/epel/6/x86_64/epel-release-6-8.noarch.rpm
RUN rpm -ivh http://rpms.famillecollet.com/enterprise/remi-release-6.rpm

# rbenvのインストール
RUN yum -y install gcc git tar openssl openssl-devel zlib-devel readline-devel curl-devel
RUN git clone https://github.com/sstephenson/rbenv.git /root/.rbenv
RUN git clone https://github.com/sstephenson/ruby-build.git /root/.rbenv/plugins/ruby-build
RUN ./root/.rbenv/plugins/ruby-build/install.sh
ENV PATH /root/.rbenv/bin:$PATH
RUN echo 'eval "$(rbenv init -)"' >> /etc/profile.d/rbenv.sh \
 && echo 'eval "$(rbenv init -)"' >> .bashrc
 
# rubyのインストール
ENV CONFIGURE_OPTS --disable-install-doc
ADD ./ruby-versions.txt /root/ruby-versions.txt
RUN xargs -L 1 rbenv install < /root/ruby-versions.txt
ADD ./ruby-versions-use.txt /root/ruby-versions-use.txt
RUN xargs rbenv global < /root/ruby-versions-use.txt

# apache, phpのインストール
RUN yum install -y --enablerepo=epel,remi,remi-php56 httpd httpd-devel libmcrypt php php-devel php-pear php-mbstring php-xml php-pdo php-mcrypt php-gd

# vimのインストール
RUN yum -y install mercurial ncurses-devel make gcc perl-ExtUtils-Embed python-devel lua lua-devel
RUN cd /usr/local/src \
 && hg clone https://bitbucket.org/vim-mirror/vim vim \
 && cd vim \
 && ./configure \
	--enable-multibyte \
	--with-features=huge \
	--enable-perlinterp \
	--enable-pythoninterp \
	--enable-luainterp=dynamic \
	--enable-rubyinterp \
 && make \
 && make install
RUN mkdir -p ~/.vim/bundle \
 && git clone https://github.com/Shougo/neobundle.vim ~/.vim/bundle/neobundle.vim
RUN mkdir -p ~/.vim/colors \
 && cd ~/.vim/colors \
 && git clone https://github.com/w0ng/vim-hybrid.git \
 && mv vim-hybrid/colors/hybrid.vim ~/.vim/colors/
RUN cd ~/ \
 && git clone https://abettie:@bitbucket.org/abettie/dotfiles.git dotfiles \
 && sh ~/dotfiles/dotfilesLink.sh

# デーモンを動かす準備
RUN yum -y install python-setuptools 
RUN easy_install supervisor
