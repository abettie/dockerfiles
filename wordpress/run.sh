#!/bin/bash

sudo docker run --restart=always -d -p 8085:80 --network=isolated_nw --ip=172.18.0.3 \
-v ~/docker_share/wordpress:/docker_share \
--name wordpress wordpress:4.9.1-php7.2-apache
