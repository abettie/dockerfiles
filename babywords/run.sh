#!/bin/bash

sudo docker run --restart=always -d -p 8084:80 --net=isolated_nw -v ~/docker_share/babywords:/docker_share --name babywords myi_babywords
