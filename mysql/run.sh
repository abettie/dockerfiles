#!/bin/bash

sudo docker run --restart=always --name mysql -p 3306:3306 --network=isolated_nw --ip=172.18.0.100 -v /home/toshi/Dockerfiles/mysql/conf:/etc/mysql/conf.d -v /home/toshi/Dockerfiles/mysql/sql:/docker-entrypoint-initdb.d -v ~/docker_share/mysql:/docker_share -e MYSQL_ROOT_PASSWORD=rootpass -d mysql:5.7
