create database bwdb;
GRANT ALL PRIVILEGES ON *.* TO toshi@'172.%' IDENTIFIED BY 'jimmy' WITH GRANT OPTION;
GRANT ALL ON bwdb.* TO bwuser@'172.%' IDENTIFIED BY 'buraburacho';
GRANT ALL ON bwdb_test.* TO bwuser_t@'172.%' IDENTIFIED BY 'buraburacho';
GRANT ALL ON bwdb_dev.* TO bwuser_d@'172.%' IDENTIFIED BY 'buraburacho';
FLUSH PRIVILEGES;

